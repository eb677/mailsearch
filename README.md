UIS Lookup mail search script
=============================

This is a script designed for use with Mutt and NeoMutt's `query_command`
feature, for looking up email addresses from a directory.  In this case, the
script will go to [UIS Lookup](https://www.lookup.cam.ac.uk/) and search there.

The only dependency is Python 3.  This is a design goal, so that the script can
easily be installed on a system without needing to install any other packages.


Getting started as a Mutt/NeoMutt user
--------------------------------------

- Clone this repository or download [`mailsearch.py`](mailsearch.py).
- Add this to your `.muttrc`:
  ```
  set query_command = '/path/to/mailsearch.py'
  ```

If you download the script to a location in your $PATH, you can just specify
the `query_command` as `mailsearch.py`.


Command-line usage
------------------

The script is also useful as a CLI tool for querying Lookup, and the plaintext
format is easily human-readable even though it's mainly meant to be
machine-readable.

```
$ ./mailsearch.py Roman
1 result
S.P.Q. Romanus	spqr2@cam.ac.uk	staff    Society for Putting Things On Top of Other Things
```

Note that the search term must be a single argument, so it needs quoting if it
contains whitespace characters:

```
$ ./mailsearch.py "S P Q Romanus"
```


Advantages of the Lookup web API over LDAP
------------------------------------------

Many existing solutions for directory search might involve querying Lookup's
LDAP interface instead -- this is something that is generally well-supported
among mail clients, and in Mutt's case might involve a popular script such as
[mutt-ldap](https://github.com/wking/mutt-ldap).

However, Lookup has extra features only available over the 'native' web API:

- Advanced [LQL](https://www.lookup.cam.ac.uk/lql) queries
- Listing of all a person's affiliated institutions, rather than just their
  'primary' institution
- Inclusion of 'cancelled' records (people who have left the University) in
  search results
- Association of a person's attributes (e.g. role, telephone number) with their
  affiliated institutions [currently ignored by this script]
- Possibly quicker queries (I haven't tested this thoroughly)

Currently `mailsearch.py` behaves as if the first argument passed to it was
entered into the Lookup online interface search box with 'include cancelled
entries'.  It will try a second time with 'approximate matches' enabled if
there are no results the first time round.


Glossary
--------

- **Jackdaw**: the UIS central user administration database.  It stores, among
  other things, one version of a person's name, _one_ 'primary' institutional
  affiliation, and college affiliation (if any).

- **Lookup**: the UIS online directory for the university.  It can store a lot
  more information than Jackdaw, including a user-editable version of their
  name (which is used in this script), preferred email address, and multiple
  institutional affiliations.

- **LDAP**: Lightweight Directory Access Protocol, a standard protocol intended
  for use in organisations that store any kind of hierarchical directory (e.g.
  information on people)


Copyright and licensing
-----------------------

Copyright 2018 to 2020, Edwin Bahrami Balani.

This is open source software: see [`COPYING`](COPYING) or the script header for
licence conditions.
