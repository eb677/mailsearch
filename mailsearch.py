#!/usr/bin/env python3
"""Usage: mailsearch <search term>"""

# Copyright (c) Edwin Bahrami Balani, 2018 to 2020
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.)

import json
import sys

from urllib.parse import urlencode, urljoin, quote
from urllib.request import urlopen, Request

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(message)s')

LOOKUP_API = "https://www.lookup.cam.ac.uk/api/v1/"


def objprop(key, wrapper_cls=None, *, single=False, default=None):
    if wrapper_cls:
        logging.debug("Creating objprop %s with wrapper %r", key, wrapper_cls)
        @property
        def fetcher(self):
            logging.debug("Fetching property %s", key)
            stuff = self._data.get(key)
            if stuff:
                if not single:
                    logging.debug(" -> found, wrapping multiple items with %r",
                                  wrapper_cls)
                    return [wrapper_cls(mem) for mem in stuff]
                else:
                    logging.debug(" -> found, wrapping single items with %r",
                                  wrapper_cls)
                    return wrapper_cls(stuff)
            else:
                logging.debug(" -> not found, returning configured default %r",
                              default)
                return default
        return fetcher
    else:
        logging.debug("Creating objprop %s without wrapper", key)
        return property(lambda self: self._data.get(key, default))


class APIError(Exception):
    def __init__(self, status, message, details=None):
        self.status = status
        self.message = message
        self.details = details

    def __str__(self):
        return "{0}: {1}".format(self.status, self.message)

    @classmethod
    def from_api_errordata(cls, data):
        e = cls(data['status'], data['message'], data['details'])


class SchemaObject:
    def __init__(self, data):
        self._data = data


class SchemeValueMixin:
    scheme  = objprop('scheme')
    value   = objprop('value')

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return '<%s %s:%s>' % (self.__class__.__name__, self.scheme, self.value)


class Attribute(SchemaObject, SchemeValueMixin):
    attrid  = objprop('attrid')
    comment = objprop('comment')

    def __repr__(self):
        return '<%s %s %s:%s>' % (self.__class__.__name__, self.attrid,
                                  self.scheme, self.value)


class Identifier(SchemaObject, SchemeValueMixin):
    pass


class AttributeMixin:
    attributes = objprop('attributes', Attribute)


def attrib(scheme, default=None):
    @property
    def wrapper(self):
        if self.attributes:
            for attr in self.attributes:
                if attr.scheme == scheme:
                    return attr.value
        return default
    return wrapper


class CommonPropertiesMixin:
    cancelled = objprop('cancelled')


class Group(SchemaObject, CommonPropertiesMixin):
    groupid = objprop('groupid')
    name = objprop('name')
    title = objprop('title')
    description = objprop('description')
    email = objprop('email')

    def __repr__(self):
        return "<%s %s %r>" % (self.__class__.__name__, self.groupid, self.name)


class Institution(SchemaObject, AttributeMixin, CommonPropertiesMixin):
    instid = objprop('instid')
    name = objprop('name')
    acronym = objprop('acronym')
    description = objprop('description')
    email = objprop('email')

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return '<%s %s %r>' % (self.__class__.__name__, self.instid, self.name)


class Person(SchemaObject, AttributeMixin, CommonPropertiesMixin):
    id_             = objprop('identifier', Identifier, single=True)
    visible_name    = objprop('visibleName')
    email           = attrib('email')
    instid          = attrib('jdInstid')
    institutions    = objprop('institutions', Institution)
    college         = attrib('jdCollege')
    mis_status      = attrib('misAffiliation')
    staff           = objprop('staff')
    student         = objprop('student')

    @property
    def primary_inst(self, can_fail=False):
        if not self.institutions:
            return None
        if not self.instid:
            return None
        for i in self.institutions:
            if i.instid == self.instid:
                return i
        logging.debug("person's jdInstid does not match any of the person's "
                      "listed institutions -- returning their first inst")
        return self.institutions[0]

    def __repr__(self):
        return "<%s %s %r>" % (self.__class__.__name__, self.groupid, self.name)


# Cross-referential class properties are defined after initial class definition
# to avoid circular dependency issues
Group.members       = objprop('members', Person)
Institution.members = objprop('members', Person)
Person.groups       = objprop('groups', Group)
Institution.parents = objprop('parentInsts', Institution)
Institution.children = objprop('childInsts', Institution)


class IbisClient:
    def __init__(self, base=None):
        self.base = base or LOOKUP_API

    def _get(self, path, params={}):
        """GET a Lookup API endpoint with the configured base"""
        qstring = urlencode(params)
        url = urljoin(self.base, path)
        if qstring:
            url += '?' + qstring
        req = Request(url, headers={'Accept': 'application/json'})
        with urlopen(req) as resp:
            jsonstring = resp.read().decode()
            data = json.loads(jsonstring)
            if resp.status != 200:
                raise APIError(data)
            else:
                return data

    def _post(self, path, params={}):
        """POST to a Lookup API endpoint with the configured base"""
        raise NotImplementedError

    def search_people(self, expr, approx=False):
        params = {
            'includeCancelled': 'true',
            'limit': '1000',
            'query': expr,
            'fetch': 'all_attrs,all_insts,all_groups'
        }
        if approx:
            params['approxMatches'] = True
        data = self._get("person/search", params)
        return [Person(i) for i in data['result']['people']]


def print_row(p):
    default_email = p.id_.value + '@cam.ac.uk'

    email = p.email or default_email
    name = p.visible_name

    details = []
    if p.cancelled:
        details.append('CANCELLED')
    if p.email and (p.id_.value != p.email.split('@')[0]):
        # Email address local part is not the person's CRSid, so we add the
        # CRSid to the details section for convenience
        details.append(p.id_.value)
    affiliation_detail_map = {
        (True, True): 'sta+stud',
        (True, False): 'staff',
        (False, True): 'student',
        (False, False): 'nostatus',
    }
    if p.college:
        details.append(p.college)
    if p.primary_inst:
        details.append(p.primary_inst.name)

    detail_string = '%-8s %s' % (
        affiliation_detail_map[(p.staff, p.student)],
        ', '.join(details)
    )
    print(email, name, detail_string, sep='\t')


def print_results(unsorted_results, approx):
    if unsorted_results:
        results = sorted(unsorted_results, key=lambda p: p.cancelled)

        result_message = '%d result' % len(results)
        if len(results) > 1:
            result_message += 's'
        if approx:
            result_message += ' (using approximate matching)'
        print(result_message)

        for r in results:
            print_row(r)
        return 0
    else:
        print("No results")
        return 1


def main():
    if len(sys.argv) < 2:
        print(__doc__)
        return 2

    search_term = sys.argv[1]
    client = IbisClient()

    approx = False
    results = client.search_people(search_term)

    if not results:
        approx = True
        results = client.search_people(search_term, approx=approx)

    return print_results(results, approx)

if __name__ == '__main__':
    sys.exit(main())
